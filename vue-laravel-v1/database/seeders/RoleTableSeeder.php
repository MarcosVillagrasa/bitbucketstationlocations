<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role= new \App\Models\Role([
            'nombre_role'=>'administrador',
        ]);
        $role -> save();

        $role= new \App\Models\Role([
            'nombre_role'=>'user',
        ]);
        $role -> save();
    }
}
