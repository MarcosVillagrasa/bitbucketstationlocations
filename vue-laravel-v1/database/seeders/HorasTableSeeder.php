<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HorasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>1,
            'horas'=>'23:40',
            'dias'=>'miércoles 13/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>2,
            'horas'=>'18:30',
            'dias'=>'jueves 14/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>3,
            'horas'=>'17:40',
            'dias'=>'miercoles 13/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>4,
            'horas'=>'19:00',
            'dias'=>'lunes 18/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>5,
            'horas'=>'21:00',
            'dias'=>'sábado 16/01' ,
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>6,
            'horas'=>'22:00',
            'dias'=>'domingo 16/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>7,
            'horas'=>'22:30',
            'dias'=>'viernes 15/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>8,
            'horas'=>'20:40',
            'dias'=>'viernes 15/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>1,
            'horas'=>'16:10',
            'dias'=>'viernes 15/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>1,
            'horas'=>'19:10',
            'dias'=>'sábado 16/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>1,
            'horas'=>'20:00',
            'dias'=>'domingo 17/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>9,
            'horas'=>'18:25',
            'dias'=>'viernes 15/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>10,
            'horas'=>'21:10',
            'dias'=>'sábado 16/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>11,
            'horas'=>'14:00',
            'dias'=>'domingo 17/01',
        ]);
        $hora -> save();


        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>12,
            'horas'=>'16:10',
            'dias'=>'viernes 15/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>13,
            'horas'=>'18:10',
            'dias'=>'sábado 16/01',
        ]);
        $hora -> save();

        $hora= new \App\Models\Horas([
            'tipoPeliculas'=>14,
            'horas'=>'22:10',
            'dias'=>'domingo 17/01',
        ]);
        $hora -> save();

    }
}
