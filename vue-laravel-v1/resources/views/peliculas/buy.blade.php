@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 card" style="background-color: #b0c8f3">
                <div class="col-6">
                    <a class="navbar-brand" href="{{ url('/peliculas') }}" style="color: black">
                        Películas
                    </a><span class="navbar-brand">/</span>
                    <a class="navbar-brand" href="{{ route('peliculas.details',$peliculas) }}" style="color: black">
                        {{ $peliculas->titulo }}
                    </a>
                </div>
            </div>
            <div class="col-12 mt-2">
                <h1 class="text-center">{{$peliculas->titulo}}</h1>

                <hr>
            </div>


            <div class="col-12 row border border-primary rounded py-3">

                <div class="col-lg-6 col-md-6 col-sm-12 text-center mt-5 ">
                    <div class="container  mb-5">

                        <h3>Butacas ocupadas en Rojo</h3>

                        <h3>Butacas libres en blanco</h3>
                    </div>

                <?php

                /*utilizo la funcion butacaRandom para no repetir el codigo cada vez que quiera generar una butaca*/
                function butacaRandom($numero,$letra){

                    switch ($letra) {
                        case '1':
                            return $numero."a";
                            break;
                        case '2':
                            return $numero."b";
                            break;
                        case '3':
                            return $numero."c";
                            break;
                        case '4':
                            return $numero."d";
                            break;
                    }
                }

                /*Genero aleatoriamente el array de butacas ocupadas*/

                for ($i=0; $i <= 3; $i++) {

                    /*las variables $numero y $letra las paso a la funcion butacaRandom y el resultado de la funcion lo meto en el array*/

                    $numero = rand(1,4);
                    $letra = rand(1,4);

                    $ocupadasRandom[$i] = butacaRandom($numero,$letra);

                }

                /*Transformo un valor del array ocupadas Random, haciendo una butaca fija*/

                $ocupadasRandom[1] = "2b";

                /*Genero un array con el numero y letra de cada butaca*/

                for ($i=1; $i <= 4; $i++) {
                    for ($j=1; $j <= 4; $j++) {
                        switch ($j) {
                            case 1:
                                $butaca[$i-1][$j-1] = $i."a";
                                break;
                            case 2:
                                $butaca[$i-1][$j-1] = $i."b";
                                break;
                            case 3:
                                $butaca[$i-1][$j-1] = $i."c";
                                break;
                            case 4:
                                $butaca[$i-1][$j-1] = $i."d";
                                break;
                        }
                    }
                }

                //genero las imagenes con las butacas libres y ocupadas

                for ($i=3; $i >= 0; $i--) {

                    //este switch es para escribir las letras que sirven como coordenadas

                    switch ($i) {
                        case '3':
                            echo "D";
                            break;
                        case '2':
                            echo "C";
                            break;
                        case '1':
                            echo "B";
                            break;
                        case '0':
                            echo "A";
                            break;
                            break;
                    }

                    for ($j=0; $j <= 3; $j++) {

                        //este if muestra las imagenes de las butacas con colores (comprada en gris, libre en blanco, ocupada en rojo)

                        //si la butaca se encuentra en el array de ocupadasRandom quiere decir que esta ocupada

                        if($butaca[$j][$i] == $ocupadasRandom[0] || $butaca[$j][$i] == $ocupadasRandom[1] || $butaca[$j][$i] == $ocupadasRandom[2] || $butaca[$j][$i] == $ocupadasRandom[3]){
                            //ocupada
                            print "<img src='/images/butacas/butaca_ocupada.png'>";
                        }else{
                            //libre
                            print "<img src='/images/butacas/butaca_vacia.png'>";
                        }
                    }
                    echo "<br>";
                }

                //este echo es para escribir los numeros que sirven como coordenadas
                echo "<pre style='font-size:16px'>    1    2    3    4 </pre>";

                ?>

                    <div>Una entrada son 5.67€</div>
                </div>

                <form class="col- form-group" action="ticket" method="get">

                    <!--Creo un input oculto y serializo el array de las butacas ocupadas para enviarlo con el formulario-->
                    <input type="hidden" name="ocupadas" value='<?php echo serialize($ocupadasRandom) ?>'>

                    <!--Formulario para escribir el nombre del comprador (para crear el array asociativo)-->
                    Nombre:<br>
                    <input class="form-control" type="text" name="nombre" value="{{ Auth::user()->name }}"><br>

                    <!--Cuantas palomitas quieres comprar-->
                    Cantidad de palomintas:<br>
                    <div class="row">
                        <select class="form-control col-7 ml-3" name="tipoPalomitas">
                            <option value="nada">Sin palomitas</option>
                            <option value="peque">Pequeñas (3.59€)</option>
                            <option value="mediana">Medianas (6.59€)</option>
                            <option value="grande">Grandes (10.59€)</option>
                        </select>
                        <input class="form-control col-4" type="number" name="palomitas"><br>

                    </div>
                    <br>
                    <!--Cuantos refrescos quieres comprar-->
                    Cantidad de refrescos:<br>
                    <div class="row">
                        <select class="form-control col-7 ml-3" name="tipoBebida">
                            <option value="nada">Sin refresco</option>
                            <option value="agua">Botella de agua (1L) (1.59€)</option>
                            <option value="cocacola">Coca-Cola (3.99€)</option>
                            <option value="fantaN">Fanta de naranja (3.99€)</option>
                            <option value="fantaL">Fanta de limón (3.99€)</option>
                            <option value="nestea">Nestea (3.99€)</option>
                        </select>
                        <input class="form-control col-4" type="number" name="refrescos"><br>

                    </div>
                    <br>
                    <div>
                        <details-view></details-view>
                    </div>
                    <br>
                    <span>Seleccione el día de la película</span>
                    <br>

                    <select  class="form-control text-center" name="hora">
                    @foreach($horas as $hora)

                        @if($peliculas->id==$hora->tipoPeliculas)

                            <option value="{{$hora->dias}} {{$hora->horas}}">{{$hora->dias}} {{$hora->horas}}</option>

                        @endif
                    @endforeach
                    </select>
                    <br>
                    <!--Formulario para elegir que butaca queremos comprar-->
                    Selecciona tu butaca:<br>
                    <select class="form-control" name="butaca" value="butaca">
                        <option value="numero">Numero de butaca</option>
                        <option value="1a">1a</option>
                        <option value="1b">1b</option>
                        <option value="1c">1c</option>
                        <option value="1d">1d</option>
                        <option value="2a">2a</option>
                        <option value="2b">2b</option>
                        <option value="2c">2c</option>
                        <option value="2d">2d</option>
                        <option value="3a">3a</option>
                        <option value="3b">3b</option>
                        <option value="3c">3c</option>
                        <option value="3d">3d</option>
                        <option value="4a">4a</option>
                        <option value="4b">4b</option>
                        <option value="4c">4c</option>
                        <option value="4d">4d</option>
                    </select><br>

                    <span>Introduzca descuento</span>
                    <select  class="form-control text-center" name="descuento">nada
                        <option value="nada">Ninguno</option>
                        <option value="joven">Carné Joven</option>
                        <option value="uni">Carné Universitario</option>
                        <option value="cine">Descuento del cine</option>
                    </select>
                    <br>
                    <input class="btn btn-primary col-12" type="submit" name="enviar">
                </form>

            </div>
        </div>
    </div>

@endsection
