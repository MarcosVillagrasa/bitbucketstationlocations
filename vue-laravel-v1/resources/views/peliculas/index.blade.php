@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-light float-right">
                <form class="form-inline ">
                    <select name="tipo" class="form-control mr-sm-2">
                        <option value="titulo">Título</option>
                        <option value="genero">Genero</option>
                    </select>
                    <input name="buscador" class="form-control mr-sm-2" type="search" placeholder="Buscar título">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                </form>
            </nav>
        </div>
    </div>

    <div class="container">

        <div class="row">

            @foreach($peliculas as $pelicula)
                <div class="col-lg-3 col-md-4 col-xs-6" style="margin-bottom: 30px">
                    <a href="{{ route('peliculas.details',$pelicula) }}">
                        <img class="img-thumbnail"  src="images/{{ $pelicula->imagen }}" style="height: 340px"/>
                    </a>

                    <div class="row">
                        <h3 class="col-12">{{$pelicula->titulo}}</h3>
                    </div>

                    @if(Auth::check())
                        @if(Auth::user()->role_id==1)
                            <div class="row">

                                <div class="col-6">
                                    <a class="btn btn-primary navbar-brand" href="{{ route('peliculas.edit',$pelicula) }}">
                                        Actualizar
                                    </a>
                                </div>

                                <form class="col-6" method="POST" action="/peliculas/{{$pelicula->id}}">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input class="btn btn-danger" type="submit" value="Eliminar">
                                </form>
                            </div>


                        @endif
                    @endif
                </div>
                <!--<li>{{$pelicula->id}}</li>
                <li>{{$pelicula->titulo}}</li>
                <li>{{$pelicula->genero}}</li>
                <li>{{$pelicula->sinopsis}}</li>
                <li>{{$pelicula->idioma}}</li>
                <li>{{$pelicula->horas}}</li>
                <iframe width="560" height="315" src="{{$pelicula->trailer}}" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
             -->
            @endforeach

        </div>
    </div>
@endsection
