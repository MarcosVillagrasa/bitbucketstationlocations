<footer class="footer mt-auto py-3 text-center text-lg-start" style="background-color: rgb(208,227,234);
  left: 0;
  bottom: 0;
  flex-shrink: 0;
  width: 100%;
  text-align: center;">
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row">
            <!--Grid column-->
            <div class="col-lg-1">
                <img src="{{URL::to('images/Logotipo.png')}}" style="width:200px">
            </div>
            <div class="col-lg-6 col-md-12 mb-4 mb-md-0">

                <h5 class="text-uppercase">Información</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="/" class="text-dark">tucine.com</a>, Calle de mentira Nº32, 50172
                    </li>
                    <li>
                        Teléfono: <a href="/" class="text-dark">687 564 124</a>
                    </li>
                    <li>
                        Email: <a href="/" class="text-dark">tucine@tucine.com</a>
                    </li>
                    <li>
                        Empresa desarrolladora: MarcosWebs.
                    </li>
                </ul>
                <p>


                </p>
            </div>
            <div class="col-lg-2 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-0">Redes Sociales</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="https://twitter.com" class="text-dark">Twitter</a>
                    </li>
                    <li>
                        <a href="https://facebook.com" class="text-dark">Facebook</a>
                    </li>
                    <li>
                        <a href="https://instagram.com" class="text-dark">Instagram</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
</footer>

